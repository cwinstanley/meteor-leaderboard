// Set up a collection to contain player information. On the server,
// it is backed by a MongoDB collection named "players".

var Players = new Meteor.Collection("players");
var names = ["Ada Lovelace",
                   "Grace Hopper",
                   "Marie Curie",
                   "Carl Friedrich Gauss",
                   "Nikola Tesla",
                   "Claude Shannon"];

/* Client side JavaScript */
if (Meteor.isClient) {
  /* Sort the leaderboard by score */
  Template.leaderboard.players = function () {
    return Players.find({}, {sort: {score: -1, name: 1}});
  };

  Template.leaderboard.selected_name = function () {
    var player = Players.findOne(Session.get("selected_player"));
    return player && player.name;
  };

  Template.player.selected = function () {
    return Session.equals("selected_player", this._id) ? "selected" : '';
  };
  /* Button events, increase score of selected player */
  Template.leaderboard.events({
    'click input.inc': function () {
      Players.update(Session.get("selected_player"), {$inc: {score: 5}});
    },
    /* Reset all players score to random scores */
    'click input.reset': function () {
        for (var i = 0; i < names.length; i++) {
            Players.find().forEach(function( player ) {
                var rnd = Math.floor(Math.random()*10)*5;
                Players.update(player._id, {$set: {score: rnd}});
            });
        }
        /* Resort list of players */
        Template.leaderboard.players = function () {
            return Players.find({}, {sort: {score: -1, name: 1}});
        };
    }

  });

  /* Triggered when you select a user on the leaderboard */
  Template.player.events({
    'click': function () {
      Session.set("selected_player", this._id);
    }
  });
}

/* On server startup, create some players if the database is empty. */
if (Meteor.isServer) {
  /* When the server gets initiated create the list of names and random scores */
  Meteor.startup(function () {
    if (Players.find().count() === 0) {
     for (var i = 0; i < names.length; i++)
        Players.insert({name: names[i], score: Math.floor(Math.random()*10)*5});
    }
  });
}
